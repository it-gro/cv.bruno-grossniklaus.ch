#!/usr/bin/env bash

cd $(dirname $0)/..
rm -rf resources/ public/
hugo --cleanDestinationDir --gc
#rsync -rlt --delete -v --checksum --groupmap=*:www-data  public/ www1@zulu2:/srv/www/cv.bruno-grossniklaus.ch
rsync -rlt --delete -v --checksum public/ it-grossniklaus.ch:www/cv.bruno-grossniklaus.ch
