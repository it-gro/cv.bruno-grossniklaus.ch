#/bin/bash

cd $(dirname $0)/..

find . -type d | xargs chmod 755
find . -type f -print0 | xargs -0 chmod 644
find . -name "*~" | xargs rm -f
chmod 755 bin/*

rm -rf resources/ public/
